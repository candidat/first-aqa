package com.omni.firstaqa.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by TF on 07.04.2016.
 */
public class HiTechMainPage extends BaseNewsPage implements IOpenable {

    private final String searchButtonXPath = ".//../a[contains (text(), 'Поиск')]";

    public HiTechMainPage(WebDriver driver){
        super(driver);
    }

    @Override
    public void openPage(int waitSeconds){
        driver.get("https://hi-tech.mail.ru/");

        try {
            //driver.wait(waitSeconds * 1000);
            Thread.sleep(waitSeconds * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickToSearchButton(){

        WebElement element = driver.findElement(By.xpath(searchButtonXPath));
        element.click();

    }
}
