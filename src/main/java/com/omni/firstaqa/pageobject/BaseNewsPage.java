package com.omni.firstaqa.pageobject;

import org.openqa.selenium.WebDriver;

/**
 * Created by TF on 14.04.2016.
 */
public abstract class BaseNewsPage {

    public final WebDriver driver;

    public BaseNewsPage(WebDriver driver) {
        this.driver = driver;
    }
}
