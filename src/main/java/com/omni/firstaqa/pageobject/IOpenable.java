package com.omni.firstaqa.pageobject;

/**
 * Created by TF on 14.04.2016.
 */
public interface IOpenable {
    void openPage(int seconds);

}
