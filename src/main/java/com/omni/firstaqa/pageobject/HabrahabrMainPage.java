package com.omni.firstaqa.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by dnikandrov on 08/04/16.
 */
public class HabrahabrMainPage {

    private WebDriver driver;
    private final String menuButtonXpath = ".//*[@id='navbar']/div[@class = \"nav_panel\"]/a[@title=\"Меню\"]";
    private final String vakansiiItemXpath = ".//*[@id='menu_tab']/div[@class= \"menu\"]/a[7]";

    public HabrahabrMainPage(WebDriver driver) {this.driver = driver;}

    public void openPage(){
        driver.get("https://habrahabr.ru/");

        try {
            driver.wait(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickToMenuButton() {
        WebElement element = driver.findElement(By.xpath(menuButtonXpath));
        element.click();
    }

    public void clickToVakansiiButton() {
        WebElement element = driver.findElement(By.xpath(vakansiiItemXpath));

        element.click();
    }
}
