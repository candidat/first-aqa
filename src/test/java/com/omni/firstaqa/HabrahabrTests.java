package com.omni.firstaqa;

import com.omni.firstaqa.pageobject.HabrahabrMainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;

import static org.junit.Assert.assertEquals;

/**
 * Created by dnikandrov on 08/04/16.
 */
public class HabrahabrTests {

    private WebDriver driver;

    @BeforeClass
    public void initDriver() { driver = new FirefoxDriver(); }

    @AfterClass
    public void closeDriver() {driver.close();}

    @Test
    public void openVakansiiPage() {
        HabrahabrMainPage mainPage = new HabrahabrMainPage(driver);

        mainPage.openPage();
        mainPage.clickToMenuButton();
        mainPage.clickToVakansiiButton();

        String currentUrl = driver.getCurrentUrl();
        String expectedUrl = "https://moikrug.ru/";

        Assert.assertEquals(currentUrl,expectedUrl, "Test failed. Wrong page is opened.");

//        assertEquals("FAIL - correct page isn't opened - " + currentUrl, expectedUrl, currentUrl);
    }

}
