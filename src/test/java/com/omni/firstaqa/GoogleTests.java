package com.omni.firstaqa;

import com.omni.firstaqa.pageobject.GoogleMainPage;
import com.omni.firstaqa.pageobject.HiTechMainPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by T on 17.04.2016.
 */
public class GoogleTests {

    private WebDriver driver;

    @BeforeClass
    public void initDriver(){
        driver = new FirefoxDriver();
    }

    @AfterClass
    public void closeDriver(){
        driver.quit();
    }

    @Test
    public void myTest() {
        GoogleMainPage mainPage = new GoogleMainPage(driver);

//        driver.get("https://hi-tech.mail.ru/");
//        WebElement element = driver.findElement(By.xpath(".//../a[contains (text(), 'Поиск')]"));
//        element.click();

        mainPage.openPage(10);
        mainPage.clickToSearchButton();

        String currentUrl = driver.getCurrentUrl();
        String expectedUrl = "https://www.google.com/doodles";

        assertEquals("FAIL - google.ru doodles page isn't opened - " + currentUrl, expectedUrl, currentUrl);
    }
}
