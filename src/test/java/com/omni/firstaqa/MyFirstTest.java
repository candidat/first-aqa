package com.omni.firstaqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.io.Console;
import java.util.concurrent.TimeUnit;

/**
 * Created by TF on 24.03.2016.
 */
public class MyFirstTest {

    @Test
    public void myTest() {
        FirefoxDriver driver = new FirefoxDriver();
        driver.get("https://hi-tech.mail.ru/");
        WebElement element = driver.findElement(By.xpath(".//../a[contains (text(), 'Поиск')]"));
        element.click();

        String currentUrl = driver.getCurrentUrl();
        //System.out.println(currentUrl);
        String expectedUrl = "http://go.mail.ru/";

        if (currentUrl.equals(expectedUrl)) {
            System.out.println("OK - mail.ru search page opened - " + currentUrl);
        }
        else {
            System.out.println("FAIL - mail.ru search page isn't opened - " + currentUrl);
        }

        driver.quit();
    }

    public void test(String value) {}

}
